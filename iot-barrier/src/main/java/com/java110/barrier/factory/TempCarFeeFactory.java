package com.java110.barrier.factory;


import com.java110.bean.dto.coupon.ParkingCouponCarDto;
import com.java110.core.utils.DateUtil;
import com.java110.dto.carInout.CarInoutDto;
import com.java110.dto.fee.TempCarFeeConfigAttrDto;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * 临时车 费用 工厂类
 */
public class TempCarFeeFactory {

    /**
     * 判断 用户是支付完成
     *
     * @param carInoutDto
     * @return
     */
    public static boolean judgeFinishPayTempCarFee(CarInoutDto carInoutDto) {

        //不是支付完成 状态
        if (!CarInoutDto.STATE_PAY.equals(carInoutDto.getState())) {
            return false;
        }

        //支付时间是否超过15分钟
        Date payTime = null;

        payTime = carInoutDto.getPayTime();

        if(payTime == null){
            return false;
        }

        Date nowTime = DateUtil.getCurrentDate();
        //支付完成超过15分钟
        if (nowTime.getTime() - payTime.getTime() > 15 * 60 * 1000) {
            return false;
        }
        return true;
    }

    /**
     * 判断 用户是支付完成
     *
     * @param carInoutDto
     * @return
     */
    public static long getTempCarCeilMin(CarInoutDto carInoutDto, List<ParkingCouponCarDto> parkingCouponCarDtos) {

        //支付时间是否超过15分钟
        Date payTime = null;
        double min = 0.0;
        long minLong = 0;

        //不是支付完成 状态
        if (CarInoutDto.STATE_PAY.equals(carInoutDto.getState())) {
            payTime = carInoutDto.getPayTime();
        }

        if (payTime == null) {
            payTime = DateUtil.getDateFromStringA(carInoutDto.getOpenTime());
        }
        Date nowTime = DateUtil.getCurrentDate();
        //支付完成超过15分钟

        min = (nowTime.getTime() - payTime.getTime()) / (60 * 1000 * 1.00);

        //todo 支付完成 并且没有超过15分钟 则分钟数修改为0
        if ((nowTime.getTime() - payTime.getTime() < 15 * 60 * 1000)
                && CarInoutDto.STATE_PAY.equals(carInoutDto.getState()) ) {
            min = 0;
        }

        minLong = new Double(Math.ceil(min)).longValue();


        if (parkingCouponCarDtos == null || parkingCouponCarDtos.size() < 1) {
            return minLong;
        }

        long couponMin = 0;
        for (ParkingCouponCarDto parkingCouponCarDto : parkingCouponCarDtos) {
            if (!ParkingCouponCarDto.TYPE_CD_HOURS.equals(parkingCouponCarDto.getTypeCd())
                    && !ParkingCouponCarDto.TYPE_CD_FREE.equals(parkingCouponCarDto.getTypeCd())) {
                continue;
            }

            double value = Math.ceil(Double.parseDouble(parkingCouponCarDto.getValue()));

            couponMin = new Double(value).longValue();

            if (ParkingCouponCarDto.TYPE_CD_FREE.equals(parkingCouponCarDto.getTypeCd())) {
                couponMin = 24 * 60;
            }

            if (minLong == 0) {
                break;
            }
            parkingCouponCarDto.setHasUser(true);
            if (minLong > couponMin) {
                minLong = minLong - couponMin;
            } else {
                minLong = 0;
                break;
            }
        }

        return minLong;
    }

    /**
     * 判断 用户是支付完成
     *
     * @param carInoutDto
     * @return
     */
    public static long getTempCarMin(CarInoutDto carInoutDto) {

        //支付时间是否超过15分钟
        Date payTime = null;
        double min = 0.0;
        try {
            //不是支付完成 状态
            if (CarInoutDto.STATE_PAY.equals(carInoutDto.getState())) {
                payTime = carInoutDto.getPayTime();
            } else {
                payTime = DateUtil.getDateFromString(carInoutDto.getInTime(), DateUtil.DATE_FORMATE_STRING_A);
            }
            Date nowTime = DateUtil.getCurrentDate();
            //支付完成超过15分钟

            return (nowTime.getTime() - payTime.getTime()) / (60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static double getAttrValueDouble(List<TempCarFeeConfigAttrDto> tempCarFeeConfigAttrDtos, String specCd) {

        for (TempCarFeeConfigAttrDto tempCarFeeConfigAttrDto : tempCarFeeConfigAttrDtos) {
            if (tempCarFeeConfigAttrDto.getSpecCd().equals(specCd)) {
                return Double.parseDouble(tempCarFeeConfigAttrDto.getValue());
            }
        }

        return 0.0;
    }


    public static String getAttrValueString(List<TempCarFeeConfigAttrDto> tempCarFeeConfigAttrDtos, String specCd) {
        for (TempCarFeeConfigAttrDto tempCarFeeConfigAttrDto : tempCarFeeConfigAttrDtos) {
            if (tempCarFeeConfigAttrDto.getSpecCd().equals(specCd)) {
                return tempCarFeeConfigAttrDto.getValue();
            }
        }
        return "";
    }

    public static int getAttrValueInt(List<TempCarFeeConfigAttrDto> tempCarFeeConfigAttrDtos, String specCd) {
        for (TempCarFeeConfigAttrDto tempCarFeeConfigAttrDto : tempCarFeeConfigAttrDtos) {
            if (tempCarFeeConfigAttrDto.getSpecCd().equals(specCd)) {
                return Integer.parseInt(tempCarFeeConfigAttrDto.getValue());
            }
        }
        return 0;
    }
}
