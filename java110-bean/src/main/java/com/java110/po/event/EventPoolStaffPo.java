package com.java110.po.event;

import com.java110.bean.dto.PageDto;

import java.io.Serializable;
import java.util.Date;

public class EventPoolStaffPo implements Serializable {
    private String epsId;
    private String eventId;
    private String staffId;
    private String staffName;
    private String eventReason;
    private String eventResult;
    private Integer eventHours;
    private String communityId;
    private String state;
    private String statusCd = "0";

    public String getEpsId() {
        return epsId;
    }

    public void setEpsId(String epsId) {
        this.epsId = epsId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getEventReason() {
        return eventReason;
    }

    public void setEventReason(String eventReason) {
        this.eventReason = eventReason;
    }

    public String getEventResult() {
        return eventResult;
    }

    public void setEventResult(String eventResult) {
        this.eventResult = eventResult;
    }

    public Integer getEventHours() {
        return eventHours;
    }

    public void setEventHours(Integer eventHours) {
        this.eventHours = eventHours;
    }

    public String getCommunityId() {
        return communityId;
    }

    public void setCommunityId(String communityId) {
        this.communityId = communityId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
