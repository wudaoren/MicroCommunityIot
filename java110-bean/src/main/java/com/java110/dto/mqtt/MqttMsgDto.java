package com.java110.dto.mqtt;

import java.io.Serializable;

public class MqttMsgDto implements Serializable{

    private String topic;
    private String msg;

    private String taskId;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
