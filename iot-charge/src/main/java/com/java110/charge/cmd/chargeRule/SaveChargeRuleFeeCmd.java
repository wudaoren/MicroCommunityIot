/*
 * Copyright 2017-2020 吴学文 and java110 team.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.java110.charge.cmd.chargeRule;

import com.alibaba.fastjson.JSONObject;
import com.java110.bean.ResultVo;
import com.java110.core.annotation.Java110Cmd;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.cmd.Cmd;
import com.java110.core.cmd.CmdEvent;
import com.java110.core.context.ICmdDataFlowContext;
import com.java110.core.exception.CmdException;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.core.utils.Assert;
import com.java110.core.utils.BeanConvertUtil;
import com.java110.core.utils.DateUtil;
import com.java110.doc.annotation.*;
import com.java110.intf.charge.IChargeRuleFeeV1InnerServiceSMO;
import com.java110.po.chargeRuleFee.ChargeRuleFeePo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * 类表述：保存
 * 服务编码：chargeRuleFee.saveChargeRuleFee
 * 请求路劲：/app/chargeRuleFee.SaveChargeRuleFee
 * add by 吴学文 at 2023-03-18 22:25:35 mail: 928255095@qq.com
 * open source address: https://gitee.com/wuxw7/MicroCommunity
 * 官网：http://www.homecommunity.cn
 * 温馨提示：如果您对此文件进行修改 请不要删除原有作者及注释信息，请补充您的 修改的原因以及联系邮箱如下
 * // modify by 张三 at 2021-09-12 第10行在某种场景下存在某种bug 需要修复，注释10至20行 加入 20行至30行
 */

@Java110CmdDoc(title = "添加充电计费规则",
        description = "用于外系统添加充电计费规则",
        httpMethod = "post",
        url = "http://{ip}:{port}/iot/api/chargeRule.saveChargeRuleFee",
        resource = "chargeDoc",
        author = "吴学文",
        serviceCode = "chargeRule.saveChargeRuleFee",
        seq = 6
)

@Java110ParamsDoc(params = {
        @Java110ParamDoc(name = "communityId", length = 30, remark = "小区ID"),
        @Java110ParamDoc(name = "ruleId", length = 64, remark = "规则ID"),
        @Java110ParamDoc(name = "chargeType", length = 8, remark = "充电类型"),
        @Java110ParamDoc(name = "durationPrice", length = 10, remark = "小时电价"),
})

@Java110ResponseDoc(
        params = {
                @Java110ParamDoc(name = "code", type = "int", length = 11, defaultValue = "0", remark = "返回编号，0 成功 其他失败"),
                @Java110ParamDoc(name = "msg", type = "String", length = 250, defaultValue = "成功", remark = "描述"),
        }
)

@Java110ExampleDoc(
        reqBody = "{\"ruleId\":\"123\",\"communityId\":\"2022081539020475\",\"chargeType\":\"1001\",\"durationPrice\":\"1.00\"}",
        resBody = "{'code':0,'msg':'成功'}"
)
@Java110Cmd(serviceCode = "chargeRule.saveChargeRuleFee")
public class SaveChargeRuleFeeCmd extends Cmd {

    private static Logger logger = LoggerFactory.getLogger(SaveChargeRuleFeeCmd.class);

    public static final String CODE_PREFIX_ID = "10";

    @Autowired
    private IChargeRuleFeeV1InnerServiceSMO chargeRuleFeeV1InnerServiceSMOImpl;

    @Override
    public void validate(CmdEvent event, ICmdDataFlowContext cmdDataFlowContext, JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "ruleId", "请求报文中未包含ruleId");
        Assert.hasKeyAndValue(reqJson, "durationPrice", "请求报文中未包含durationPrice");
        Assert.hasKeyAndValue(reqJson, "communityId", "请求报文中未包含communityId");
        Assert.hasKeyAndValue(reqJson, "chargeType", "请求报文中未包含chargeType");
        if ("1001".equals(reqJson.getString("chargeType"))) {
            Assert.hasKeyAndValue(reqJson, "minEnergyPrice", "请求报文中未包含minEnergyPrice");
            Assert.hasKeyAndValue(reqJson, "maxEnergyPrice", "请求报文中未包含maxEnergyPrice");
        } else if ("1002".equals(reqJson.getString("chargeType"))) {
            Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
            Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");
        }
    }

    @Override
    @Java110Transactional
    public void doCmd(CmdEvent event, ICmdDataFlowContext cmdDataFlowContext, JSONObject reqJson) throws CmdException {
        String startTime = reqJson.getString("startTime");
        reqJson.remove("startTime");
        String endTime = reqJson.getString("endTime");
        reqJson.remove("endTime");

        ChargeRuleFeePo chargeRuleFeePo = BeanConvertUtil.covertBean(reqJson, ChargeRuleFeePo.class);

        if (startTime != null && endTime != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            try {
                chargeRuleFeePo.setStartTime(new Time(sdf.parse(startTime).getTime()));
                chargeRuleFeePo.setEndTime(new Time(sdf.parse(endTime).getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        chargeRuleFeePo.setCrfId(GenerateCodeFactory.getGeneratorId(CODE_PREFIX_ID));
        int flag = chargeRuleFeeV1InnerServiceSMOImpl.saveChargeRuleFee(chargeRuleFeePo);

        if (flag < 1) {
            throw new CmdException("保存数据失败");
        }

        cmdDataFlowContext.setResponseEntity(ResultVo.success());
    }
}
