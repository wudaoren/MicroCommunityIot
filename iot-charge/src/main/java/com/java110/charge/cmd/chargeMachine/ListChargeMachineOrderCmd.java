/*
 * Copyright 2017-2020 吴学文 and java110 team.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.java110.charge.cmd.chargeMachine;

import com.alibaba.fastjson.JSONObject;
import com.java110.bean.ResultVo;
import com.java110.core.annotation.Java110Cmd;
import com.java110.core.cmd.Cmd;
import com.java110.core.cmd.CmdEvent;
import com.java110.core.context.ICmdDataFlowContext;
import com.java110.core.exception.CmdException;
import com.java110.core.utils.Assert;
import com.java110.core.utils.BeanConvertUtil;
import com.java110.doc.annotation.*;
import com.java110.dto.chargeMachine.ChargeMachineOrderDto;
import com.java110.intf.charge.IChargeMachineOrderV1InnerServiceSMO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;


/**
 * 类表述：查询
 * 服务编码：chargeMachineOrder.listChargeMachineOrder
 * 请求路劲：/app/chargeMachineOrder.ListChargeMachineOrder
 * add by 吴学文 at 2023-03-08 02:24:15 mail: 928255095@qq.com
 * open source address: https://gitee.com/wuxw7/MicroCommunity
 * 官网：http://www.homecommunity.cn
 * 温馨提示：如果您对此文件进行修改 请不要删除原有作者及注释信息，请补充您的 修改的原因以及联系邮箱如下
 * // modify by 张三 at 2021-09-12 第10行在某种场景下存在某种bug 需要修复，注释10至20行 加入 20行至30行
 */

@Java110CmdDoc(title = "查询充电订单",
        description = "用于外系统查询充电订单",
        httpMethod = "get",
        url = "http://{ip}:{port}/iot/api/chargeMachine.listChargeMachineOrder",
        resource = "chargeDoc",
        author = "吴学文",
        serviceCode = "chargeMachine.listChargeMachineOrder",
        seq = 17
)

@Java110ParamsDoc(params = {
        @Java110ParamDoc(name = "page", type = "int", length = 11, remark = "页数"),
        @Java110ParamDoc(name = "row", type = "int", length = 1, remark = "行数"),
        @Java110ParamDoc(name = "communityId", length = 30, remark = "小区ID"),
})

@Java110ResponseDoc(
        params = {
                @Java110ParamDoc(name = "code", type = "int", length = 11, defaultValue = "0", remark = "返回编号，0 成功 其他失败"),
                @Java110ParamDoc(name = "msg", type = "String", length = 250, defaultValue = "成功", remark = "描述"),
                @Java110ParamDoc(name = "data", type = "Array", remark = "有效数据"),
                @Java110ParamDoc(parentNodeName = "data", name = "orderId", type = "String", remark = "订单号"),
                @Java110ParamDoc(parentNodeName = "data", name = "machineId", type = "String", remark = "充电桩ID"),
                @Java110ParamDoc(parentNodeName = "data", name = "portId", type = "String", remark = "插槽ID"),
                @Java110ParamDoc(parentNodeName = "data", name = "personName", type = "String", remark = "用户名"),
                @Java110ParamDoc(parentNodeName = "data", name = "personTel", type = "String", remark = "手机号"),
                @Java110ParamDoc(parentNodeName = "data", name = "amount", type = "String", remark = "扣款金额"),
                @Java110ParamDoc(parentNodeName = "data", name = "acctDetailId", type = "String", remark = "扣款账号"),
                @Java110ParamDoc(parentNodeName = "data", name = "chargeHours", type = "String", remark = "充电小时"),
                @Java110ParamDoc(parentNodeName = "data", name = "energy", type = "String", remark = "充电量"),
                @Java110ParamDoc(parentNodeName = "data", name = "state", type = "String", remark = "插槽状态"),
        }
)

@Java110ExampleDoc(
        reqBody = "http://{ip}:{port}/iot/api/chargeMachine.listChargeMachineOrder?page=1&row=10&communityId=123123",
        resBody = "{'code':0,'msg':'成功','data':[{'orderId':'123','machineId':'123','portId':'123','personName':'李四','personTel':'13345674567','amount':'2.70','acctDetailId':'123123','chargeHours':'1.00','energy':'1.00','state':'1001'}]}"
)
@Java110Cmd(serviceCode = "chargeMachine.listChargeMachineOrder")
public class ListChargeMachineOrderCmd extends Cmd {

  private static Logger logger = LoggerFactory.getLogger(ListChargeMachineOrderCmd.class);
    @Autowired
    private IChargeMachineOrderV1InnerServiceSMO chargeMachineOrderV1InnerServiceSMOImpl;

    @Override
    public void validate(CmdEvent event, ICmdDataFlowContext cmdDataFlowContext, JSONObject reqJson) {
        super.validatePageInfo(reqJson);
        Assert.hasKeyAndValue(reqJson, "communityId", "communityId不能为空");
    }

    @Override
    public void doCmd(CmdEvent event, ICmdDataFlowContext cmdDataFlowContext, JSONObject reqJson) throws CmdException {

           ChargeMachineOrderDto chargeMachineOrderDto = BeanConvertUtil.covertBean(reqJson, ChargeMachineOrderDto.class);

           int count = chargeMachineOrderV1InnerServiceSMOImpl.queryChargeMachineOrdersCount(chargeMachineOrderDto);

           List<ChargeMachineOrderDto> chargeMachineOrderDtos = null;

           if (count > 0) {
               chargeMachineOrderDtos = chargeMachineOrderV1InnerServiceSMOImpl.queryChargeMachineOrders(chargeMachineOrderDto);
           } else {
               chargeMachineOrderDtos = new ArrayList<>();
           }

           ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) reqJson.getInteger("row")), count, chargeMachineOrderDtos);

           ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

           cmdDataFlowContext.setResponseEntity(responseEntity);
    }
}
