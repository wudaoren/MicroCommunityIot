package com.java110.community.dao;

import java.util.List;
import java.util.Map;

/**
 * @ClassName IRoomsTreeServiceDao
 * @Description TODO
 * @Author wuxw
 * @Date 2020/10/15 22:10
 * @Version 1.0
 * add by wuxw 2020/10/15
 **/
public interface IRoomsTreeServiceDao {


    List<Map>  queryRoomsTree(Map info);

}
