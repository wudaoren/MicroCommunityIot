/**
    入驻小区
**/
(function(vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            chargeMonthOrderManageInfo: {
                chargeMonthOrders: [],
                total: 0,
                records: 1,
                moreCondition: false,
                orderId: '',
                conditions: {
                    cardId: '',
                    personName: '',
                    personTel: '',
                    communityId: '',
                    primeRate: '',
                    receivableAmount: '',
                    receivedAmount: '',
                    communityId: vc.getCurrentCommunity().communityId,
                    queryStartTime:'',
                    queryEndTime:''
                }
            }
        },
        _initMethod: function() {
            $that._listChargeMonthOrders(DEFAULT_PAGE, DEFAULT_ROWS);

            vc.initDateTime('queryStartTime',function(_value){
                $that.chargeMonthOrderManageInfo.conditions.queryStartTime = _value;
            });
            vc.initDateTime('queryEndTime',function(_value){
                $that.chargeMonthOrderManageInfo.conditions.queryEndTime = _value;
            });
        },
        _initEvent: function() {

            vc.on('chargeMonthOrderManage', 'listChargeMonthOrder', function(_param) {
                $that._listChargeMonthOrders(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function(_currentPage) {
                $that._listChargeMonthOrders(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listChargeMonthOrders: function(_page, _rows) {

                $that.chargeMonthOrderManageInfo.conditions.page = _page;
                $that.chargeMonthOrderManageInfo.conditions.row = _rows;
                let param = {
                    params: $that.chargeMonthOrderManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/chargeCard.listChargeMonthOrder',
                    param,
                    function(json, res) {
                        let _json = JSON.parse(json);
                        $that.chargeMonthOrderManageInfo.total = _json.total;
                        $that.chargeMonthOrderManageInfo.records = _json.records;
                        $that.chargeMonthOrderManageInfo.chargeMonthOrders = _json.data;
                        vc.emit('pagination', 'init', {
                            total: $that.chargeMonthOrderManageInfo.records,
                            currentPage: _page,
                            dataCount:_json.total
                        });
                    },
                    function(errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddChargeMonthOrderModal: function() {
                vc.emit('addChargeMonthOrder', 'openAddChargeMonthOrderModal', {});
            },
            _queryChargeMonthOrderMethod: function() {
                $that._listChargeMonthOrders(DEFAULT_PAGE, DEFAULT_ROWS);
            },
            _moreCondition: function() {
                if ($that.chargeMonthOrderManageInfo.moreCondition) {
                    $that.chargeMonthOrderManageInfo.moreCondition = false;
                } else {
                    $that.chargeMonthOrderManageInfo.moreCondition = true;
                }
            },
            _settingOrderCard: function() {
                vc.jumpToPage('/#/pages/charge/chargeMonthCardManage')
            }
        }
    });
})(window.vc);